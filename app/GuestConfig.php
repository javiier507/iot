<?php

namespace IoT;

use Illuminate\Database\Eloquent\Model;

class GuestConfig extends Model
{
    protected $table = 'guest_configs';

    protected $hidden = ['password', 'created_at', 'updated_at'];

    public function setPasswordAttribute($password)
    {
        if (!empty($password))
        {
            $this->attributes['password'] = bcrypt($password);
        }
    }
}
