<?php

namespace IoT\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use IoT\User;
use IoT\Article;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function destroy(User $user, Article $article)
    {
        if( $user->role == 1 ) {
            return true;
        }
        else {
            return $user->id === $article->user_id;    
        }        
    }
}
