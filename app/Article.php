<?php

namespace IoT;

use Illuminate\Database\Eloquent\Model;

use DB;

class Article extends Model
{    
    protected $table = 'articles';

    protected $fillable = ['title', 'content', 'image', 'category', 'user_id'];

    protected $hidden = ['remember_token'];

    public function user()
    {
        return $this->belongsTo('IoT\User');
    }
}