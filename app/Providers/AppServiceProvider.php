<?php

namespace IoT\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;
use Hash;
use IoT\GuestConfig;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('guest_password', function($attribute, $value, $parameters, $validator) {
            $guestConfig = GuestConfig::find(1);
            if (Hash::check($value, $guestConfig->password)) {
                return true;
            }
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
