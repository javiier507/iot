<?php

namespace IoT;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'users';

    protected $fillable = ['firstname', 'lastname', 'username', 'email', 'password', 'image', 'phrase'];

    protected $hidden = ['password', 'remember_token', 'created_at', 'updated_at'];

    public function getFullNameAttribute() 
    {
        return $this->firstname.' '.$this->lastname;
    }

    public function setPasswordAttribute($password)
    {
        if (!empty($password))
        {
            $this->attributes['password'] = bcrypt($password);
        }
    }

    public function setImageAttribute($image)
    {
        $name = Carbon::now()->second.$image->getClientOriginalName();
        $this->attributes['image'] = $name;
        \Storage::disk('local')->put($name, \File::get($image));
    }

    public function article()
    {
        return $this->hasMany('IoT\Article');
    }
}
