<?php

namespace IoT\Http\Middleware;

use Closure;

class SuperAdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if( $request->user()->role != 1 )
        {
            abort(403, "No Tienes Permiso Para Esta Sección");
        }
        return $next($request);
    }
}
