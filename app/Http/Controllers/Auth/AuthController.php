<?php

namespace IoT\Http\Controllers\Auth;

use IoT\User;
use Validator;
use IoT\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'firstname' => 'required|max:50',
            'lastname' => 'required|max:50',
            'username' => 'required|max:25|unique:users',
            'email' => 'required|email|max:50|unique:users',
            'password' => 'required|confirmed|min:6|max:60',
            'phrase' => 'required|max:200',
            'image' => 'required|max:100',
            'password_guest' => 'required|guest_password',
        ]);
    }

    protected function create(array $data)
    {
        return User::create($data);
    }

    //agregados
    protected $redirectPath = '/administracion';

    protected function loginPath()
    {
        return route('login');
    }

    protected function logoutPath()
    {
        return route('logout');
    }
}