<?php

namespace IoT\Http\Controllers;

use Illuminate\Http\Request;
//use IoT\Http\Requests;
use IoT\Http\Controllers\Controller;

use IoT\Article;
use Illuminate\Routing\Route;
//use DB;
use IoT\Http\Requests\ArticleRequest;

use IoT\Repositories\ArticleRepository;

class ArticleController extends Controller
{
    protected $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->beforeFilter('@findArticle', ['only' => ['show', 'edit', 'update']]);
        $this->articleRepository = $articleRepository;
    }

    public function findArticle(Route $route)
    {
        $this->article = Article::find($route->getParameter('id'));
    }

    private function readArticles()
    {
        return $this->articleRepository->read();   
    }

    public function index()
    {
        return view('blog.article-index', ['articles'=>$this->readArticles()]);
    }

    public function indexAdmin()
    {
        return view('admin.article-index', ['articles'=>$this->readArticles()]);
    }

    public function create()
    {
        return view('admin.article-create');
    }

    public function store(ArticleRequest $request)
    {
        $article = $request->all();
        $user_id = $request->user()->id;    
        $this->articleRepository->create($article, $user_id);
        return redirect('/administracion');
    }

    public function show($id)
    {
        return view('blog.article-show', ['article' => $this->article]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy(Article $article)
    {
        $this->authorize('destroy', $article);
        $article->delete();
        return response()->json(['respuesta' => $article]);
    }

    public function category($name = "concepto")
    {
        $articles = $this->articleRepository->showCategories($name);
        if($articles=='404')
        {
            abort(404);
        }

        return view('blog.article-category', ['articles'=>$articles, 'category'=>$name]);
    }
    
    public function cache()
    {
        if (!\Cache::has('registro')) 
        {
            $articles = $this->readArticles();
            \Cache::put('registro', $articles, 5);
        }

        $articles = \Cache::get('registro');
        return view('blog.article-index', compact('articles'));
    }
}
