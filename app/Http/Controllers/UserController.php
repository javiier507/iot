<?php

namespace IoT\Http\Controllers;

use Illuminate\Http\Request;
use IoT\Http\Requests;
use IoT\Http\Controllers\Controller;

use IoT\User;
use Hash;
use IoT\GuestConfig;

class UserController extends Controller
{
    public function __construct()
    {
         $this->middleware('superadmin', ['only' => ['guest', 'guestConfig']]);
    }

    public function index()
    {
        $users = User::all();
        return view('blog.user-index', compact('users'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit(Request $request)
    {
        $user = $request->user();
        return view('admin.user-update', compact('user'));
    }

    public function update(Request $request)
    {
        $user = User::find($request->user()->id);
        $user->firstname = $request->firstname;
        $user->lastname = $request->lastname;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->phrase = $request->phrase;
        $user->save();

        return redirect('/editar-usuario')->with('mensaje', 'Datos Actualizado');
    }

    public function destroy($id)
    {
        //
    }

    public function ubicacion()
    {
        return view('blog.ubication');
    }

    public function guest()
    {
        return view('admin.guestconfig-update');
    }

    public function guestConfig(Request $request)
    {
        $guestConfig = GuestConfig::find(1);
        if( $request->passwordNew == $request->passwordRepeat )
        {
            if (Hash::check($request->passwordCurrent, $guestConfig->password))
            {
                $guestConfig->password = $request->passwordNew;
                $guestConfig->save();
                return  redirect()->route('guestConfig')
                    ->with('mensaje', 'Contraseña Actualizada');
            }
            else
            {
                return  redirect()->route('guestConfig')
                    ->with('mensajeAlert', 'Contraseña Actual Incorrecta');
            }
        }
        else
        {
            return  redirect()->route('guestConfig')
                ->with('mensajeAlert', 'Contraseña Nueva y Repetida No Coinciden');
        }
    }
}
