<?php

namespace IoT\Http\Controllers;

use Illuminate\Http\Request;
use IoT\Http\Requests;
use IoT\Http\Controllers\Controller;

use Mail;

class EmailController extends Controller
{
    public function __construct()
    {
         $this->middleware('auth');
    }

    public function getEmail()
    {
    	return view('emails.contact');
    }

    public function postEmail(Request $request)
    {
    	Mail::send('emails.message', $request->all(), function($message) use ($request)
    	{
    		$message->from($request->email, $request->name); 
	        $message->subject('Laravel-Message');
 			$message->to(env('CONTACT_MAIL'), env('CONTACT_NAME'));
    	});

    	return redirect('/contacto')->with('mensaje', 'Correo Enviado');
    }
}