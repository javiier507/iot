<?php

//blog
Route::get('/', 'ArticleController@index');
Route::get('articulo/{id}', 'ArticleController@show');
Route::get('categoria/{name?}', 'ArticleController@category')->name('category');
Route::get('equipo', 'UserController@index')->name('team');
Route::get('ubicacion', 'UserController@ubicacion')->name('ubication');
Route::get('contacto', 'EmailController@getEmail');
Route::post('contacto', 'EmailController@postEmail');
//Route::get('cache', 'ArticleController@cache');

//autenticacion
Route::get('inicio-sesion', 'Auth\AuthController@getLogin');
Route::post('inicio-sesion', 'Auth\AuthController@postLogin')->name('login');
Route::get('salida', 'Auth\AuthController@getLogout')->name('logout');
Route::get('registro', 'Auth\AuthController@getRegister');
Route::post('registro', 'Auth\AuthController@postRegister')->name('register');
Route::get('contrasena/correo', 'Auth\PasswordController@getEmail');
Route::post('contrasena/correo', 'Auth\PasswordController@postEmail');
Route::get('contrasena/reiniciar/{token}', 'Auth\PasswordController@getReset');
Route::post('contrasena/reiniciar', 'Auth\PasswordController@postReset');

/*Route::get('error', function(){ 
    abort(503);
});*/

// administracion
Route::group(['middleware' => 'auth'], function()
{
	Route::get('administracion', 'ArticleController@indexAdmin');
	Route::get('articulo-nuevo', 'ArticleController@create')->name('articulo-nuevo');
	Route::post('articulo-nuevo', 'ArticleController@store');
	Route::delete('articulo/{article}', 'ArticleController@destroy');
	Route::get('editar-usuario', 'UserController@edit');
	Route::put('editar-usuario', 'UserController@update')->name('update');
	Route::get('configuracion-invitados', 'UserController@guest')->name('guestConfig');
	Route::post('configuracion-invitados', 'UserController@guestConfig');
});