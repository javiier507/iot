<?php

namespace IoT\Http\Requests;

use IoT\Http\Requests\Request;

class ArticleRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'title' => 'required | min:4 | max:75',
            'content' => 'required | min:4 |max:1000',
            'category' => 'required | min:4 |max:50'
        ];
    }

    public function messages()
    {
         return [
            'title.required' => 'El campo Titulo es requerido',
            'title.min' => 'El campo Titulo no puede tener menos de 4 carácteres',
            'title.max' => 'El campo Titulo no puede tener más de 75 carácteres',

            'content.required' => 'El campo Contenido es requerido',
            'content.min' => 'El campo Contenido no puede tener menos de 4 carácteres',
            'content.max' => 'El campo Contenido no puede tener más de 1000 carácteres',

            'category.required' => 'El campo Categoria es requerido!',
            'category.min' => 'El campo Categoria no puede tener menos de 4 carácteres',
            'category.max' => 'El campo Categoria no puede tener más de 50 carácteres'
        ];
    }
}
