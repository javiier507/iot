<?php

namespace IoT\Repositories;
 
use IoT\Article;
use DB;

class ArticleRepository
{
	public function create($article, $user_id)
	{
	    $category = $article['category'];
	    $image = "concepto.jpg";
	    
	    if($category=="maquina-a-maquina") 
	    {
	        $image = "m2m.jpg";    
	    }
	    elseif($category=="ipv6")
	    {
	        $image = "ipv6.jpg";
	    }
	    elseif($category=="seguridad") 
	    {
	        $image = "seguridad.jpg";
	    }
	    else
	    {
	        $image = "concepto.jpg";   
	    }

	    DB::table('articles')->insert([
	        'title' => $article['title'], 
	        'content' => $article['content'],
	        'category' => $category,
	        'image' => $image,
	        'user_id' => $user_id
	    ]);
	}

	public function read()
	{
		$articles = DB::select('select articles.id, articles.title, articles.content, articles.category, 
	        articles.image, users.username from articles inner join users 
	        on articles.user_id = users.id order by articles.id desc');
		return $articles;
	}

	public function showCategories($name)
	{
	    $articles = null;

	    if($name=="concepto")
	    {
	        $articles = DB::table('articles')
	        ->where('category', '=', 'concepto')->orderBy('id', 'desc')->get();
	    }
	    elseif($name=="maquina-a-maquina")
	    {
	        $articles = DB::table('articles')
	        ->where('category', '=', 'maquina-a-maquina')->orderBy('id', 'desc')->get();
	    }
	    elseif($name=="ipv6")
	    {
	        $articles = DB::table('articles')
	        ->where('category', '=', 'ipv6')->orderBy('id', 'desc')->get();
	    }
	    elseif($name=="seguridad")
	    {
	        $articles = DB::table('articles')
	        ->where('category', '=', 'seguridad')->orderBy('id', 'desc')->get();
	    }
	    else
	    {
	        $articles = "404";
	    }

	    return $articles;
	}
}