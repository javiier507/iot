<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        //DB::table('users')->truncate();

        DB::table('users')->insert([
            [
                'firstname' => 'carlos',
                'lastname' => 'javier',
                'username' => 'javiier507',
                'email' => 'javiier507@hotmail.com',
                'password' => bcrypt('secret'),
                'role' => 1,
                'image' => 'javiier507.jpg',
                'phrase' => 'Desarrollor Encargado'
            ],
            [
                'firstname' => 'tony',
                'lastname' => 'stark',
                'username' => 'ironman',
                'email' => 'javiidev@gmail.com',
                'password' => bcrypt('secret'),
                'role' => 2,
                'image' => 'ironman.jpg',
                'phrase' => 'Genio, Millonario, Playboy, Filántropo... ¬¬'
            ]
        ]);
    }
}