<?php

use Illuminate\Database\Seeder;

class ArticleTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('articles')->truncate();
        factory('IoT\Article', 6)->create();
    }
}
