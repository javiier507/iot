<?php

use Illuminate\Database\Seeder;

class GuestConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('guest_configs')->insert([
        	'password' => bcrypt('secret123')
        ]);
    }
}
