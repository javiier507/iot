<?php

$factory->define(IoT\Article::class, function (Faker\Generator $faker) 
{
    return [
        'title' => $faker->sentence($nbWords = 3),
        'content' => $faker->text($maxNbChars = 100),
        'image' => $faker->randomElement($array = ['concepto.jpg', 'm2m.jpg', 'ipv6.jpg', 'seguridad.jpg']),
        'category' => $faker->randomElement($array = ['concepto', 'maquina-a-maquina', 'ipv6', 'seguridad']),
        'user_id' => $faker->randomElement($array = [1, 2])
    ];
});