angular
.module('app')
.controller('homeController', ['$scope', 'Articulos', homeController])
.controller('teamController', ['$scope', 'Autores', teamController])
.controller('mapController', ['$scope', mapController])

function homeController($scope, Articulos)
{	
	$scope.articulos = Articulos;
}

function teamController($scope, Autores)
{	
	$scope.autores = Autores;
}

function mapController($scope)
{
	
}