$(document).ready(inicio);

function inicio()
{
	$('.btn-eliminar').click(eliminar);
}

function eliminar()
{
	var opcion = confirm("Desea Eliminar El Articulo ?");
	if(opcion==true)
	{
		var fila = $(this).parents(".registro"),
		id = fila.data('id'),
		route = '/articulo/'+id,
		token = $("#token").val();

		$.ajax({
			url: route,
			headers: {'X-CSRF-TOKEN': token},
			type: 'DELETE',
			dataType: 'JSON',
			data: id,
			success: function(respuesta)
			{
				console.log(respuesta);
				setTimeout(function(){
					fila.remove();
				}, 2000);
			}
		});
	}
}