<div class="mensajes">
    @if($errors->any())
        <div class="alert alert-danger" role="alert">
            <p>Atencion !</p>
            <ol>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ol>
        </div>
    @endif
    @if (session('mensaje'))
        <div class="alert alert-success">
            <p>{{ session('mensaje') }}</p>
        </div>
    @endif
    @if (session('mensajeAlert'))
        <div class="alert alert-danger">
            <p>{{ session('mensajeAlert') }}</p>
        </div>
    @endif
</div>