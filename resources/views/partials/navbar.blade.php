<nav class="navbar navbar-default navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
    		<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" 
    		data-target="#navegacion">
    			<span class="sr-only">Toggle navigation</span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
    		</button>
    		<a class="navbar-brand" href="administracion">Administracion</a>
    	</div>
    	<div class="collapse navbar-collapse" id="navegacion">
    		<ul class="nav navbar-nav navbar-right" role="tablist">
    			<li><a href="{{ route('articulo-nuevo') }}">Crear Articulo</a></li>
                <li><a href="{{ route('guestConfig') }}">Configuracion Invitados</a></li>
    			<li class="dropdown">
    				<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" 
    				aria-haspopup="true" aria-expanded="false">
    					{{ Auth::user()->username }}<span class="caret"></span>
    				</a>
					<ul class="dropdown-menu">
                        <li><a href="{{ route('update') }}">Editar Datos</a></li>
						<li><a href="{{ route('logout') }}">Salir</a></li>
					</ul>		    				
    			</li>
    		</ul>
    	</div>
	</div>
</nav>