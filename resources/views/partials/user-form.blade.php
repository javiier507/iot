<div class="form-group">
    {!! Form::label('firstname', 'Nombre') !!}
    {!! Form::text('firstname', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('lastname', 'Apellido') !!}
    {!! Form::text('lastname', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('username', 'Usuario') !!}
    {!! Form::text('username', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('email', 'Correo') !!}
    {!! Form::email('email', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('phrase', 'Frase') !!}
    {!! Form::text('phrase', null, ['class'=>'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('image', 'Imagen') !!}
    {!! Form::file('image', null, ['class'=>'form-control']) !!}
</div>