@extends('layouts.master')

@section('title')
    Equipo
@endsection

@section('content')
	@if($users)		
		<div class="mdl-grid" style="margin-bottom:65px">
			@foreach($users as $user)
  				<div class="mdl-cell mdl-cell--3-col mdl-cell--4-col-tablet mdl-cell--12-col-phone">  				
			  		<div class="mdl-card mdl-shadow--2dp card-user">
					  	<div class="mdl-card__title card-user-title">
					    	<p class="mdl-card__title-text card-user-name">{{$user->username}}</p>
					  	</div>
					  	<div class="mdl-card__media">
					    	<img src="/uploads/{{$user->image}}" width="100%" height="100x">
					  	</div>
					  	<div class="mdl-card__supporting-text">
					    	<p>{{$user->phrase}}</p>
					  	</div>
					</div>				
  				</div>
  			@endforeach
		</div>
	@endif
@endsection