@extends('layouts.master')

@section('title')
    Articulo
@endsection

@section('content')
	@if($article)
	    <div class="mdl-card mdl-shadow--4dp card-article">
	  	    <div class="mdl-card__title">
	    	    <h2 class="mdl-card__title-text">{{$article->title}}</h2>
	  	    </div>
	  	    <div class="mdl-card__media">
	    	    <img src="/assets/img/{{$article->image}}" width="100%" height="200x">
	  	    </div>
	  	    <div class="mdl-card__supporting-text">
	    	    <p>{!!$article->content!!}</p>
	  	    </div>
	  	    <div class="mdl-card__actions mdl-card--border">
	  	    	<a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" 
	  	    		href="/categoria/{{$article->category}}">{{$article->category}}
		    	</a>
	    	    <a class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href="#">
	    	    	{{$article->user->username}}
	    	    </a>
	  	    </div>
	  	    <div id="disqus_thread" style="padding:2%"></div>
	    </div>
	@endif
@endsection

@section('scripts')
	<script>
		(function() { 
			var d = document, s = d.createElement('script');
			s.src = '//internet-of-thing.disqus.com/embed.js';
			s.setAttribute('data-timestamp', +new Date());
			(d.head || d.body).appendChild(s);
		})();
	</script>
	<noscript>
		Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a>
	</noscript>
@endsection