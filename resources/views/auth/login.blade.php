@extends('layouts.admin')

@section('title')
    Iniciar Sesión
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			{!! Form::open(['route' => 'login']) !!}
				<div class="form-group">
					{!! Form::label('email', 'Correo') !!}
    				{!! Form::email('email', null, ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password', 'Contraseña') !!}
    				{!! Form::password('password', ['class'=>'form-control']) !!}
				</div>
				{!! Form::submit('Ingresar', ['class'=>'btn btn-primary', 'name'=>'login']) !!}
				<a href="/contrasena/correo" class="btn btn-info">Recuperar Contraseña</a>
			{!! Form::close() !!}
			<br/>
			@include('partials.messages')
		</div>
	</div>
@endsection