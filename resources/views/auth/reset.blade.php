@extends('layouts.admin')

@section('title')
    Reiniciar Contraseña
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			{!! Form::open(['url' => 'contrasena/reiniciar']) !!}
				<input type="hidden" name="token" value="{{ $token }}">
				<div class="form-group">
					{!! Form::label('email', 'Correo') !!}
    				{!! Form::email('email', null, ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password', 'Contraseña') !!}
    				{!! Form::password('password', ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password_confirmation', 'Repetir Contraseña') !!}
    				{!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
				</div>
				{!! Form::submit('Reiniciar', ['class'=>'btn btn-primary', 'name'=>'reiniciar']) !!}
			{!! Form::close() !!}
			<br/>
			@include('partials.messages')
		</div>
	</div>
@endsection