@extends('layouts.admin')

@section('title')
    Registro
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			{!! Form::open(['route' => 'register', 'files' => true]) !!}
				@include('partials.user-form')
				<div class="form-group">
    				{!! Form::label('password', 'Password') !!}
    				{!! Form::password('password', ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password_confirmation', 'Confirmar Contraseña') !!}
					{!! Form::password('password_confirmation', ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('password_guest', 'Contraseña de Invitado') !!}
					{!! Form::password('password_guest', ['class'=>'form-control']) !!}
				</div>
				{!! Form::submit('Guardar', ['class'=>'btn btn-primary', 'name'=>'register']) !!}
				{!! Form::reset('Restablecer', ['class'=>'btn btn-default']) !!}
			{!! Form::close() !!}
			<br/>
			@include('partials.messages')
		</div>
	</div>
@endsection