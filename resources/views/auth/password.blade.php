@extends('layouts.admin')

@section('title')
    Recuperar Contraseña
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			{!! Form::open(['url' => 'contrasena/correo']) !!}
				<div class="form-group">
					{!! Form::label('email', 'Introduce tu dirección de correo') !!}
    				{!! Form::email('email', null, ['class'=>'form-control']) !!}
				</div>
				{!! Form::submit('Recuperar', ['class'=>'btn btn-primary', 'name'=>'recuperar']) !!}
			{!! Form::close() !!}
			<br/>
			@include('partials.messages')
		</div>
	</div>
@endsection