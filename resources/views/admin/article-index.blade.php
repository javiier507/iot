@extends('layouts.admin')

@section('head')
    @parent
    {!! Html::style('/assets/css/dataTables.bootstrap.css') !!}
@endsection

@section('content')
	<div class="row">
        <div class="col-lg-12">
            @if($articles)
                <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
                <div class="panel panel-primary">
                    <div class="panel-heading">Lista Articulos</div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="users-table">
                                <thead>
                                    <th>Id</th>
                                    <th>Titulo</th>
                                    <th>Categoria</th>
                                    <th>Autor</th>
                                    <th>Ver</th>
                                    <th>Eliminar</th>
                                </thead>
                                <tbody>
                                    @foreach($articles as $article)
                                        <tr data-id="{{$article->id}}" class="registro">
                                            <td>{{$article->id}}</td>
                                            <td>{{$article->title}}</td>
                                            <td>{{$article->category}}</td>
                                            <td>{{$article->username}}</td>
                                            <td>
                                                <a class="btn btn-info" href="/articulo/{{$article->id}}" role="button">Ver</a>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-eliminar">Eliminar</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>                     
                    </div>
                </div>
            @else
                <div class="alert alert-warning">Datos Vacios!</div>
            @endif
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    {!! Html::script('/assets/js/jquery.dataTables.min.js') !!}
	{!! Html::script('/assets/js/dataTables.bootstrap.js') !!}
	{!! Html::script('/ang/admin.js') !!}
    <script>
        $(document).ready(function() {
            $('#users-table').DataTable();
        });
    </script>
@endsection