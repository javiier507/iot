@extends('layouts.admin')

@section('title')
	Articulo Nuevo
@endsection

@section('head')
    @parent
    <link href="/assets/css/font-awesome.min.css" rel="stylesheet">
@endsection

@section('content')
    <div class="panel panel-primary nuevo-articulo" ng-app="app">
        <div class="panel-heading">
            <h3 class="panel-title">Nuevo Articulo</h3>
        </div>
        <div class="panel-body">
            <form method="POST" action="{{ route('articulo-nuevo') }}">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Titulo</label>
                            <input type="text" name="title" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label>Categoria</label>
                            <select name="category" class="form-control">
                                <option value="concepto">Concepto</option>
                                <option value="maquina-a-maquina">Maquina-a-Maquina</option>
                                <option value="ipv6">IPV6</option>
                                <option value="seguridad">Seguridad</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12" ng-controller="homeController">
                        <div text-angular="text-angular" name="content" ng-model="content" 
                            ta-disabled='disabled'></div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-lg-12">
                        <button type="submit" class="btn btn-primary">Publicar</button>
                    </div>
                </div>
            </form>
            <br>
            @include('partials.messages')
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    <script src='/assets/js/angular-1.2.4.min.js'></script>
    <script src='/assets/js/angular-sanitize-1.2.4.min.js'></script>
    <script src='/assets/js/textAngular-1.1.2.min.js'></script>
    <script src="/ang/app.js"></script>
@endsection