@extends('layouts.admin')

@section('title')
	Editar Configuración de Invitados
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Editar Configuración de Invitados</h3>
        </div>
        <div class="panel-body">
            {!! Form::open(['route' => 'guestConfig']) !!}
                {!! Form::label('passwordCurrent', 'Contraseña Actual') !!}
                {!! Form::password('passwordCurrent', ['class'=>'form-control']) !!}
                {!! Form::label('passwordNew', 'Contraseña Nueva') !!}
                {!! Form::password('passwordNew', ['class'=>'form-control']) !!}
                {!! Form::label('passwordRepeat', 'Repetir Contraseña') !!}
                {!! Form::password('passwordRepeat', ['class'=>'form-control']) !!}
                <br>
                {!! Form::submit('Guardar', ['class'=>'btn btn-primary', 'name'=>'save']) !!}
                {!! Form::reset('Restablecer', ['class'=>'btn btn-default']) !!}
            {!! Form::close() !!}
            <br>
            @include('partials.messages')
        </div>
    </div>
@endsection