@extends('layouts.admin')

@section('title')
	Editar Usuario
@endsection

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title">Editar Datos de {{$user->username}}</h3>
        </div>
        <div class="panel-body">
            {!! Form::model($user, ['route' => ['update'], 'method' => 'PUT']) !!}
                @include('partials.user-form')
                {!! Form::submit('Guardar', ['class'=>'btn btn-primary', 'name'=>'editar']) !!}
                {!! Form::reset('Restablecer', ['class'=>'btn btn-default']) !!}
            {!! Form::close() !!}
            <br>
            @include('partials.messages')
        </div>
    </div>
@endsection