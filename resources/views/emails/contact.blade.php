@extends('layouts.admin')

@section('title')
    Contacto
@endsection

@section('content')
	<div class="row">
		<div class="col-lg-6 col-lg-offset-3">
			{!! Form::open(['url' => '/contacto']) !!}
				<div class="form-group">
					{!! Form::label('name', 'Nombre') !!}
    				{!! Form::text('name', null, ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('email', 'Correo') !!}
    				{!! Form::email('email', null, ['class'=>'form-control']) !!}
				</div>
				<div class="form-group">
					{!! Form::label('content', 'Mensaje') !!}
    				{!! Form::textarea('content', null, ['class'=>'form-control']) !!}
				</div>				
				{!! Form::submit('Enviar', ['class'=>'btn btn-primary', 'name'=>'enviar']) !!}
			{!! Form::close() !!}
			<br/>
			@include('partials.messages')
		</div>
	</div>
@endsection