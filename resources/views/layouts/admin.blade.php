<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>@yield('title', 'Administracion')</title>
		@section('head')
		{!! Html::style('/assets/css/bootstrap-flaty.min.css') !!}
		{!! Html::style('/assets/css/app.css') !!}
		@show
	</head>
	<body class="body-admin">
		@if (Auth::check())
			@include('partials.navbar')
		@endif
		<div class="container">@yield('content')</div>
		@section('scripts')
		{!! Html::script('/assets/js/jquery-2.1.1.min.js') !!}
		{!! Html::script('/assets/js/bootstrap.min.js') !!}
		@show
	</body>
</html>